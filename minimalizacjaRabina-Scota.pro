#-------------------------------------------------
#
# Project created by QtCreator 2013-11-25T08:50:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = minimalizacjaRabina-Scota
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    graf.cpp \
    wierzcholek.cpp \
    krawedz.cpp

HEADERS  += mainwindow.h \
    graf.h \
    wierzcholek.h \
    krawedz.h

FORMS    += mainwindow.ui
