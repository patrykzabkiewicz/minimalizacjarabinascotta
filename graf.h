#ifndef GRAF_H
#define GRAF_H

#include <QList>
#include "wierzcholek.h"

class Graf
{
public:
    Graf(int iloscWierzcholkow, int iloscStanow);
    Graf(Graf *g);
    QList<Wierzcholek> *listaWierzcholkow;
    int iloscWierzcholkow;
    int iloscStanow;

    void optymalizacja(QString fin);
    bool sprawdzStany(int a, int b);
};

#endif // GRAF_H
