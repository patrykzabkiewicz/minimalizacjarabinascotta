#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionZako_cz_triggered()
{
    this->close();
}

void MainWindow::on_pushButton_clicked()
{
    iloscWierzcholkow = ui->spinBox->value();
    iloscStanow = ui->spinBox_2->value();

    ui->tableWidget->setRowCount(iloscWierzcholkow);
    ui->tableWidget->setColumnCount(iloscStanow);

    Graf graf(iloscWierzcholkow,iloscStanow);
    for(int i=0; i < iloscWierzcholkow; i++) {
        Wierzcholek w;
        w.name = QString('A'+i);

        for(int j=0; j < iloscStanow; j++) {
            Krawedz k;
            k.stan = QString::number(j);
            k.wynik = QString('A'+rand()%iloscWierzcholkow);
            w.listaKrawedzi->append(k);
        }

        graf.listaWierzcholkow->append(w);
    }

    ui->lineEdit->setText(QString('A'));
    ui->lineEdit_2->setText(QString('A'+iloscWierzcholkow-1));

    this->wTabele(&graf,ui->tableWidget);
}

void MainWindow::on_pushButton_2_clicked()
{
    Graf graf(iloscWierzcholkow, iloscStanow);
    this->zTabeli(&graf, ui->tableWidget);

     //optymalizacja
    QString fin = ui->lineEdit_2->text();

    graf.optymalizacja(fin);

    //wpisanie do tabli ponizej
    this->wTabele(&graf,ui->tableWidget_2);

}

void MainWindow::zTabeli(Graf *graf, QTableWidget *table)
{
    graf->iloscWierzcholkow = table->rowCount();
    graf->iloscStanow = table->columnCount();

    for(int i=0; i< table->rowCount(); ++i) {
        Wierzcholek w;
        //w.name = QString('A'+i);
        w.name = table->verticalHeaderItem(i)->text();

        for(int j=0; j < table->columnCount(); ++j) {
            Krawedz k;
            k.stan = table->horizontalHeaderItem(j)->text();
            k.wynik = table->item(i,j)->text();
            w.listaKrawedzi->append(k);
        }

        graf->listaWierzcholkow->append(w);
    }

}

void MainWindow::wTabele(Graf *graf, QTableWidget *table)
{
    int i=0,j=0;

    table->clear();

    // etykiety dla tabel
    table->setRowCount(graf->listaWierzcholkow->size());
    table->setColumnCount(graf->listaWierzcholkow->value(0).listaKrawedzi->size());

    QStringList headerLabels;
    QStringList rowsLabels;

    foreach(Wierzcholek w , *(graf->listaWierzcholkow)) {
        rowsLabels << w.name;
    }
    foreach(Krawedz k , *(graf->listaWierzcholkow->value(0).listaKrawedzi)) {
        headerLabels << k.stan;
    }

    table->setHorizontalHeaderLabels(headerLabels);
    table->setVerticalHeaderLabels(rowsLabels);


    foreach(Wierzcholek w , *(graf->listaWierzcholkow)) {
        foreach(Krawedz k , *(w.listaKrawedzi)) {
            table->setItem(i,j, new QTableWidgetItem(k.wynik));
            j++;
        }
        j=0;
        i++;
    }
}


void MainWindow::on_pushButton_3_clicked()
{
    Graf graf(iloscWierzcholkow,iloscStanow);
    this->zTabeli(&graf,ui->tableWidget_2);
    this->wTabele(&graf,ui->tableWidget);
}
