#ifndef WIERZCHOLEK_H
#define WIERZCHOLEK_H

#include <QList>
#include "krawedz.h"

class Wierzcholek
{

public:
    Wierzcholek();
    QString name;
    QList<Krawedz> *listaKrawedzi;

    bool operator== ( Wierzcholek const &q) const {
        return name==q.name;
    }
};

#endif // WIERZCHOLEK_H
