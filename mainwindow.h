#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qglobal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <QDebug>
#include <QTableWidget>

#include "graf.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_actionZako_cz_triggered();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
    int iloscWierzcholkow;
    int iloscStanow;
    int **tablica;

    void zTabeli(Graf *graf, QTableWidget *table);
    void wTabele(Graf *graf, QTableWidget *table);

};

#endif // MAINWINDOW_H
