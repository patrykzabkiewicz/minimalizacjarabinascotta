#include "graf.h"

Graf::Graf(int iloscWierzcholkow, int iloscStanow)
{
    this->listaWierzcholkow = new QList<Wierzcholek>();
    this->iloscWierzcholkow = iloscWierzcholkow;
    this->iloscStanow = iloscStanow;
}

Graf::Graf(Graf *g)
{
    this->iloscStanow = g->iloscStanow;
    this->iloscWierzcholkow = g->iloscWierzcholkow;
    this->listaWierzcholkow = g->listaWierzcholkow;
}

void Graf::optymalizacja(QString fin)
{
    Graf *tmpGraf = new Graf(this);

    for(int a=0; a < tmpGraf->iloscWierzcholkow-1; ++a) {
        for(int j=a+1; j < tmpGraf->iloscWierzcholkow; ++j) {

            if( tmpGraf->listaWierzcholkow->at(j).name != fin &&  tmpGraf->listaWierzcholkow->at(a).name !=fin) {

                if(sprawdzStany(a,j)) {
                    Wierzcholek w,tmpW,tmpW2;
                    tmpW = w = tmpGraf->listaWierzcholkow->at(a);
                    tmpW2 = tmpGraf->listaWierzcholkow->at(j);
                    w.name += ","+tmpGraf->listaWierzcholkow->at(j).name;

                    tmpGraf->listaWierzcholkow->replace(a, w);
                    tmpGraf->listaWierzcholkow->removeAt(j);
                    tmpGraf->iloscWierzcholkow--;

                    //zmiana nazw w grafie
                    for(int q=0; q < tmpGraf->iloscWierzcholkow; ++q) {
                        for(int k=0; k < tmpGraf->iloscStanow; ++k) {

                            if( tmpGraf->listaWierzcholkow->at(q).listaKrawedzi->at(k).wynik == tmpW.name ||
                                    tmpGraf->listaWierzcholkow->at(q).listaKrawedzi->at(k).wynik == tmpW2.name ) {
                            Krawedz kr = tmpGraf->listaWierzcholkow->at(q).listaKrawedzi->at(k);
                            kr.wynik = w.name;
                            tmpGraf->listaWierzcholkow->at(q).listaKrawedzi->replace(k,kr);
                            }
                        }
                    }
                }
            }
        }
    }

    this->iloscStanow = tmpGraf->iloscStanow;
    this->iloscWierzcholkow = tmpGraf->iloscWierzcholkow;
    this->listaWierzcholkow = tmpGraf->listaWierzcholkow;
}


bool Graf::sprawdzStany(int a, int b)
{
    for(int i=0; i<iloscStanow; ++i) {
        if( this->listaWierzcholkow->at(a).listaKrawedzi->at(i).wynik != this->listaWierzcholkow->at(b).listaKrawedzi->at(i).wynik) return false;
    }
    return true;
}
